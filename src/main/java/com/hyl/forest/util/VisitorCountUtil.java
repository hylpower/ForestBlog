package com.hyl.forest.util;

import com.hyl.forest.entity.VisitorCount;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author hyl
 * @date 19/1/26 下午4:28
 */
public class VisitorCountUtil {
    private static VisitorCount visitorCount;
    private static ConcurrentHashMap<String, Integer> ipCountMap;
    private static boolean inserted=false;

    public static VisitorCount getVisitorCount(){
        return visitorCount;
    }

    public static Integer getTodayCount(){
        if(VisitorCountUtil.visitorCount == null){
            VisitorCountUtil.visitorCount = new VisitorCount();
        }
        return VisitorCountUtil.visitorCount.getCount();
    }

    public static Integer addVisitorCount(){
        if(visitorCount == null){
            visitorCount = new VisitorCount();
        }
        visitorCount.setCount(visitorCount.getCount()+1);
        return visitorCount.getCount();
    }

    public static void reset(){
        if(!inserted){
            //todo 插入数据库
        }
        visitorCount.setCount(0);
//        inserted=true;
    }

    public static void insertvisitorCount(){
        if(!inserted){
            //todo 插入数据库
        }
        visitorCount.setCount(0);
    }


}
