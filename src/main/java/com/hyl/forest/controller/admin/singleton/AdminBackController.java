package com.hyl.forest.controller.admin.singleton;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/admin/singleton")
public class AdminBackController {

    @Resource
    private HttpServletResponse response;

    @RequestMapping("")
    public String reToBackPage(){
        return "redirect:/admin/singleton/";
    }

    @RequestMapping("/")
    public ModelAndView toBackPage(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("Admin/singleton/public/AdminBackFrame");
        return modelAndView;
    }


    @RequestMapping("/getPageContent")
    @ResponseBody
    public ModelAndView getPageContent(@RequestParam(value = "page") String page){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("Admin/singleton/"+page);
        return modelAndView;
    }
}
