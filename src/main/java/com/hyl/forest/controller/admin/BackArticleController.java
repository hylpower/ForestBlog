package com.hyl.forest.controller.admin;

import com.hyl.forest.dto.ArticleParam;
import com.hyl.forest.entity.Article;
import com.hyl.forest.entity.Category;
import com.hyl.forest.entity.Tag;
import com.hyl.forest.entity.User;
import com.hyl.forest.service.ArticleService;
import com.hyl.forest.service.CategoryService;
import com.hyl.forest.service.TagService;
import com.github.pagehelper.PageInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * @author liuyanzhao
 */
@Controller
@RequestMapping("/admin/article")
public class BackArticleController {
    @Autowired
    private ArticleService articleService;

    @Autowired
    private TagService tagService;

    @Autowired
    private CategoryService categoryService;

    /**
     * 后台文章列表显示
     *
     * @return modelAndView
     */
    @RequestMapping(value = "")
    public ModelAndView index(@RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                        @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                        @RequestParam(required = false) String status) {
        ModelAndView mav = new ModelAndView();
        HashMap<String, Object> criteria = new HashMap<>(1);
        if (status == null) {
            mav.addObject("pageUrlPrefix", "/admin/article?pageIndex");
        } else {
            criteria.put("status", status);
            mav.addObject("pageUrlPrefix", "/admin/article?status=" + status + "&pageIndex");
        }
        PageInfo<Article> articlePageInfo = articleService.pageArticle(pageIndex, pageSize, criteria);
        mav.addObject("pageInfo", articlePageInfo);
        mav.setViewName("Admin/Article/index");
        return mav;
    }


    /**
     * 后台添加文章页面显示
     *
     * @return
     */
    @RequestMapping(value = "/insert")
    public String insertArticleView(Model model) {
        List<Category> categoryList = categoryService.listCategory();
        List<Tag> tagList = tagService.listTag();
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("tagList", tagList);
        return "Admin/Article/mdeditor";
    }

    @RequestMapping(value = "/mdupload")
    public String mduploadView(Model model) {
//        List<Category> categoryList = categoryService.listCategory();
//        List<Tag> tagList = tagService.listTag();
//        model.addAttribute("categoryList", categoryList);
//        model.addAttribute("tagList", tagList);
        return "Admin/Article/mdupload";
    }

    @RequestMapping(value = "/mdpicture")
    public String mdpictureView(Model model) {
//        List<Category> categoryList = categoryService.listCategory();
//        List<Tag> tagList = tagService.listTag();
//        model.addAttribute("categoryList", categoryList);
//        model.addAttribute("tagList", tagList);
        return "Admin/Article/mdpicture";
    }



    /**
     * 后台添加文章提交操作
     *
     * @param articleParam
     * @return
     */
    @RequestMapping(value = "/insertSubmit", method = RequestMethod.POST)
    public String insertArticleSubmit(HttpSession session, ArticleParam articleParam) {
        Article article = new Article();
        //用户ID
        User user = (User) session.getAttribute("user");
        if (user != null) {
            article.setArticleUserId(user.getUserId());
        }
        article.setArticleTitle(articleParam.getArticleTitle());
        article.setArticleContent(articleParam.getArticleContent());
        article.setArticleStatus(articleParam.getArticleStatus());
        //填充分类
        List<Category> categoryList = new ArrayList<>();
        if (articleParam.getArticleParentCategoryId() != null) {
            categoryList.add(new Category(articleParam.getArticleParentCategoryId()));
            if (articleParam.getArticleChildCategoryId() != null) {
                categoryList.add(new Category(articleParam.getArticleChildCategoryId()));
            }
        }

        article.setCategoryList(categoryList);
        //填充标签
        List<Tag> tagList = new ArrayList<>();
        if (articleParam.getArticleTagIds() != null) {
            for (int i = 0; i < articleParam.getArticleTagIds().size(); i++) {
                Tag tag = new Tag(articleParam.getArticleTagIds().get(i));
                tagList.add(tag);
            }
        }
        article.setTagList(tagList);

        articleService.insertArticle(article);
        return "redirect:/admin/article";
    }


    /**
     * 删除文章
     *
     * @param id 文章ID
     */
    @RequestMapping(value = "/delete/{id}")
    public void deleteArticle(@PathVariable("id") Integer id) {
        articleService.deleteArticle(id);
    }




    /**
     * 编辑文章页面显示
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/edit/{id}")
    public ModelAndView editArticleView(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView();

        Article article = articleService.getArticleByStatusAndId(null, id);
        modelAndView.addObject("article", article);
        List<Category> categoryList = article.getCategoryList();
        for(Category category: categoryList){
            if(category.getCategoryPid().equals(0)){
                modelAndView.addObject("firstCategory",category);
            }else{
                modelAndView.addObject("secondCategory",category);
            }

        }

//        List<Category> categoryList = categoryService.listCategory();
//        modelAndView.addObject("categoryList", categoryList);
        List<Tag> tags = tagService.listTagByArticleId(id);
        List<String> tagStrs = new ArrayList<>();
        for (Tag tag:tags){
            tagStrs.add(tag.getTagId().toString());
        }

        List<Tag> tagList = tagService.listTag();
        modelAndView.addObject("tagList", tagList);
        modelAndView.addObject("tags", String.join(",",tagStrs));


        modelAndView.setViewName("Admin/Article/mdeditor");
//        modelAndView.setViewName("Admin/Article/edit");
        return modelAndView;
    }


    /**
     * 编辑文章提交
     *
     * @param articleParam
     * @return
     */
    @RequestMapping(value = "/editSubmit", method = RequestMethod.POST)
    public String editArticleSubmit(ArticleParam articleParam) {
        Article article = new Article();
        article.setArticleId(articleParam.getArticleId());
        article.setArticleTitle(articleParam.getArticleTitle());
        article.setArticleContent(articleParam.getArticleContent());
        article.setArticleStatus(articleParam.getArticleStatus());
        //填充分类
        List<Category> categoryList = new ArrayList<>();
        if (articleParam.getArticleChildCategoryId() != null) {
            categoryList.add(new Category(articleParam.getArticleParentCategoryId()));
        }
        if (articleParam.getArticleChildCategoryId() != null) {
            categoryList.add(new Category(articleParam.getArticleChildCategoryId()));
        }
        article.setCategoryList(categoryList);
        //填充标签
        List<Tag> tagList = new ArrayList<>();
        if (articleParam.getArticleTagIds() != null) {
            for (int i = 0; i < articleParam.getArticleTagIds().size(); i++) {
                Tag tag = new Tag(articleParam.getArticleTagIds().get(i));
                tagList.add(tag);
            }
        }
        article.setTagList(tagList);
        articleService.updateArticleDetail(article);
        return "redirect:/admin/article";
    }


}



