package com.hyl.forest.controller.admin.api;

import com.hyl.forest.dto.ApiResult;
import com.hyl.forest.dto.TableApiResult;
import com.hyl.forest.entity.Category;
import com.hyl.forest.service.ArticleService;
import com.hyl.forest.service.CategoryService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/category/api")
public class CategoryApi {
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ArticleService articleService;

    @RequestMapping("/queryList")
    @ResponseBody
    public TableApiResult queryList(){
        TableApiResult apiResult = new TableApiResult();

        List<Category> categoryList = categoryService.listCategoryWithCount();
        apiResult.setData(categoryList);
        apiResult.setStatus(TableApiResult.SUCCESS);
        return apiResult;
    }

    @RequestMapping("/saveCategory")
    @ResponseBody
    public ApiResult saveCategory(@RequestParam Map<String,Object> params){
        ApiResult apiResult = new ApiResult();
        String cid = (String)params.get("cid");
        String pid = (String)params.get("pid");
        String categoryName = (String)params.get("categoryName");
        String categoryPid = (String)params.get("categoryPid");
        String categoryDescription = (String)params.get("categoryDescription");
        Category category = new Category();
        category.setCategoryPid(Integer.parseInt(categoryPid));
        category.setCategoryName(categoryName);
        category.setCategoryDescription(categoryDescription);
        if(StringUtils.isNotBlank(cid)&&StringUtils.isNotBlank(pid)){

        }else {
            categoryService.insertCategory(category);
        }
        apiResult.setStatus(ApiResult.SUCCESS);
        return apiResult;
    }

    @RequestMapping("/deleteCategory")
    @ResponseBody
    public ApiResult deleteCategory(@RequestParam Integer categoryId){
        ApiResult apiResult = new ApiResult();
        //若为父标签且父标签下存在子标签 则删除失败
        Category category = categoryService.getCategoryById(categoryId);
        if(category.getCategoryPid() == 0){
            List<Category> categoryList = categoryService.listCategoryByPid(categoryId);
            if(categoryList.size()>0){
                apiResult.setStatus(ApiResult.FAILURE);
                apiResult.setMsg("含有子标签,删除失败");
                return apiResult;
            }
        }

        //若该分类下有文章 则删除失败
        int count = articleService.countArticleByCategoryId(categoryId);
        if(count>0){
            apiResult.setStatus(ApiResult.FAILURE);
            apiResult.setMsg("该标签下含有文章,删除失败");
            return apiResult;
        }

        categoryService.deleteCategory(categoryId);
        apiResult.setStatus(ApiResult.SUCCESS);
        return apiResult;
    }
}
