package com.hyl.forest.controller.admin.api;

import com.hyl.forest.dto.ApiResult;
import com.hyl.forest.dto.TableApiResult;
import com.hyl.forest.entity.*;
import com.hyl.forest.enums.ArticleStatus;
import com.hyl.forest.service.ArticleService;
import com.hyl.forest.service.CategoryService;
import com.hyl.forest.service.TagService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/article/api")
public class ArticleApi {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private HttpSession session;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private TagService tagService;

    private static final String FIRST_CATEGORY = "first-category";
    private static final String SECOND_CATEGORY = "second-category";
    private static final String TAGS = "tags";


    @RequestMapping("/addSubmit")
    @ResponseBody
    @Transactional
    public ApiResult addSubmit(@RequestBody Map<String,Object> map){
        ApiResult apiResult = new ApiResult();

        Map<String,Object> field = (Map<String, Object>) map.get("field");
        String markdownContent = (String) map.get("mdContent");
        String htmlContent = (String) map.get("htmlContent");

        Article article = new Article();
        //用户ID
        User user = (User) session.getAttribute("user");
        if (user != null) {
            article.setArticleUserId(user.getUserId());
        }
        article.setArticleTitle((String) field.get("title"));
        article.setArticleContent(htmlContent);
        article.setHtmlContent(htmlContent);
        article.setMarkdownContent(markdownContent);
        article.setArticleStatus(Integer.parseInt((String) field.getOrDefault("status", ArticleStatus.PUBLISH.getValue().toString())));
        //填充分类
        List<Category> categoryList = new ArrayList<>();
        if (field.getOrDefault(FIRST_CATEGORY,null) != null) {
            categoryList.add(new Category(Integer.parseInt((String)field.get(FIRST_CATEGORY)) ));
            if (field.getOrDefault(SECOND_CATEGORY,null) != null) {
                categoryList.add(new Category(Integer.parseInt((String) field.get(SECOND_CATEGORY))));
            }
        }

        article.setCategoryList(categoryList);
        //填充标签
        List<Tag> tagList = new ArrayList<>();
        if (field.getOrDefault(TAGS,null) != null) {
            String[] articleTags = ((String)field.get(TAGS)).split(",");
            for (int i = 0; i < articleTags.length; i++) {
                Tag tag = new Tag(Integer.parseInt(articleTags[i]));
                tagList.add(tag);
            }
        }
        article.setTagList(tagList);
        articleService.insertArticle(article);

        apiResult.setStatus(ApiResult.SUCCESS);
        return apiResult;
    }

    @RequestMapping("/getCategory")
    @ResponseBody
    public ApiResult getCategory(){
        ApiResult apiResult = new ApiResult();
        List<Category> categoryList = categoryService.listCategory();
        apiResult.setData(categoryList);
        apiResult.setStatus(ApiResult.SUCCESS);
        return apiResult;
    }

    /**
     * 编辑文章提交
     *
     * @param map
     * @return
     */
    @RequestMapping(value = "/editSubmit", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    public ApiResult editArticleSubmit(@RequestBody Map<String,Object> map) {
        ApiResult apiResult = new ApiResult();
        Map<String,Object> field = (Map<String,Object>)map.get("field");

        Article article = new Article();
        article.setArticleId(Integer.parseInt((String)field.get("articleId")));
        article.setArticleTitle((String)field.get("title"));
        article.setArticleContent((String)map.get("htmlContent"));
        article.setMarkdownContent((String)map.get("mdContent"));
        article.setHtmlContent((String)map.get("htmlContent"));

        article.setArticleStatus(Integer.parseInt((String) field.getOrDefault("status", ArticleStatus.PUBLISH.getValue().toString())));
        //填充分类
        List<Category> categoryList = new ArrayList<>();
        if (field.getOrDefault(FIRST_CATEGORY,null) != null) {
            categoryList.add(new Category(Integer.parseInt((String)field.get(FIRST_CATEGORY)) ));
            if (field.getOrDefault(SECOND_CATEGORY,null) != null) {
                categoryList.add(new Category(Integer.parseInt((String) field.get(SECOND_CATEGORY))));
            }
        }
        article.setCategoryList(categoryList);
        //填充标签
        List<Tag> tagList = new ArrayList<>();
        if (field.getOrDefault(TAGS,null) != null) {
            String[] articleTags = ((String)field.get(TAGS)).split(",");
            for (int i = 0; i < articleTags.length; i++) {
                Tag tag = new Tag(Integer.parseInt(articleTags[i]));
                tagList.add(tag);
            }
        }
        article.setTagList(tagList);
        articleService.updateArticleDetail(article);
        apiResult.setStatus(ApiResult.SUCCESS);
        return apiResult;
    }

    @RequestMapping("/getTagList")
    @ResponseBody
    public ApiResult getTagList(){
        ApiResult apiResult = new ApiResult();
        List<Tag> tagList = tagService.listTag();
        apiResult.setStatus(ApiResult.SUCCESS);
        apiResult.setData(tagList);
        return apiResult;
    }

    @RequestMapping(value = "/queryList")
    @ResponseBody
    public TableApiResult queryList(@RequestParam(required = false, defaultValue = "1") Integer page,
                        @RequestParam(required = false, defaultValue = "10") Integer limit,
                        @RequestParam(required = false) String status) {
        TableApiResult apiResult = new TableApiResult();
        HashMap<String, Object> criteria = new HashMap<>(1);
        if (status != null) {
            criteria.put("status", status);
        }
        PageInfo<Article> articlePageInfo = articleService.pageArticle(page, limit, criteria);

        apiResult.setStatus(TableApiResult.SUCCESS);
        apiResult.setData(articlePageInfo.getList());
        apiResult.setCount(articlePageInfo.getTotal());
        return apiResult;
    }

    @RequestMapping(value="/batchDelete")
    @ResponseBody
    @Transactional
    public ApiResult batchDelete(@RequestParam Map<String,Object> params){
            ApiResult apiResult = new ApiResult();

            ArrayList<Integer> idList = (ArrayList<Integer>)params.get("idList");
            for(Integer id:idList){
                articleService.deleteArticle(id);
            }
            apiResult.success();
            apiResult.setMsg("删除成功");
            return apiResult;
    }
}
