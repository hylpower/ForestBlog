package com.hyl.forest.dto;

import lombok.Data;

import java.io.Serializable;


public class ApiResult implements Serializable {

    public static final String SUCCESS = "000000";
    public static final String FAILURE = "999999";

    protected String status;

    protected Object data;

    protected String msg;

    public void success(){
        this.status=SUCCESS;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
