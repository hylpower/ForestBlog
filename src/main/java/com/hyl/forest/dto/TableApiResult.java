package com.hyl.forest.dto;


import com.github.pagehelper.PageInfo;

import java.util.List;

public class TableApiResult extends ApiResult {

    public static final String SUCCESS = "0";

    private Long count;

    public Long getCount() {

        if(data instanceof PageInfo){
            return  ((PageInfo)data).getTotal();
        }else if(data instanceof List){
            return (long)(((List)data).size());
        }
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getCode() {
        return status;
    }

    @Override
    public Object getData(){
        if (data instanceof PageInfo){
            return ((PageInfo)data).getList();
        }else if(data instanceof List){
            return data;
        }
        return data;
    }

}
