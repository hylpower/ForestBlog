package com.hyl.forest.dto;

import lombok.Data;

/**
 * @author 言曌
 * @date 2017/11/30 下午7:41
 */

@Data
public class UploadFileVO {

    private String src;

    private String title;

}
