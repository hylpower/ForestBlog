package com.hyl.forest.mapper;

import com.hyl.forest.entity.VisitorCount;

/**
 * @author hyl
 * @date 19/1/26 下午5:09
 */
public interface VisitorCountMapper {
    public Integer insert(VisitorCount visitorCount);
}
