package com.hyl.forest.service.impl;

import com.hyl.forest.entity.VisitorCount;
import com.hyl.forest.mapper.VisitorCountMapper;
import com.hyl.forest.service.VisitorCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author hyl
 * @date 19/1/26 下午5:08
 */
@Service
public class VisitorContServiceImpl implements VisitorCountService {

    @Autowired
    private VisitorCountMapper visitorCountMapper;

    @Override
    public Integer insert(VisitorCount visitorCount) {
        return visitorCountMapper.insert(visitorCount);
    }
}
