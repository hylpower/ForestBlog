package com.hyl.forest.service;

import com.hyl.forest.entity.VisitorCount;

/**
 * @author hyl
 * @date 19/1/26 下午5:07
 */
public interface VisitorCountService {
    public Integer insert(VisitorCount visitorCount);

}
