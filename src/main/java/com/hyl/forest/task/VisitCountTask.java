package com.hyl.forest.task;

import com.hyl.forest.entity.VisitorCount;
import com.hyl.forest.service.VisitorCountService;
import com.hyl.forest.util.VisitorCountUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * 访问量处理定时任务
 */
@Component
@Slf4j
public class VisitCountTask {

    @Autowired
    private VisitorCountService visitorCountService;

    @Scheduled(cron = "0 0 0 * * ?")
    public void process() throws ParseException {
        log.info("开始处理每日访问量");
        VisitorCount visitorCount = new VisitorCount();
        visitorCount.setCount(VisitorCountUtil.getTodayCount());

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE,-1);
        visitorCount.setDate(calendar.getTime());

        visitorCount.setCreateTime(new Date());
        visitorCountService.insert(visitorCount);
        log.info("访问量重置");
        VisitorCountUtil.reset();
        log.info("执行结束");
    }
}
