package com.hyl.forest.entity;


import java.util.Date;

/**
 * @author hyl
 * @date 19/1/26 下午4:48
 */
public class VisitorCount {
    private Date date;
    private Integer count;
    private Date createTime;



    public VisitorCount(){
        date = new Date();
        count = 0;
        createTime=null;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getCount() {
        if(count == null){
            count=new Integer(0);
        }
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
