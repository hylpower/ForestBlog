package com.hyl.forest.listener;

import com.hyl.forest.util.VisitorCountUtil;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author hyl
 * @date 19/1/26 下午4:10
 */
@Slf4j
public class TodayVisitorListener implements HttpSessionListener {

//    private Logger log =  LoggerFactory.getLogger(this.getClass());

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        log.info("新sessiion创建");
        Integer count = VisitorCountUtil.addVisitorCount();
        log.info("当日访问数:[{}]",count);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        log.info("sessiion销毁");
    }
}
