<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<rapid:override name="title">
    - 文章列表
</rapid:override>
<rapid:override name="header-style">
    <style>
        /*覆盖 layui*/
        .layui-input {
            display: inline-block;
            width: 33.333% !important;
        }

        .layui-input-block {
            margin: 0px 10px;
        }


    </style>
</rapid:override>

<rapid:override name="content">
    <blockquote class="layui-elem-quote">
        <span class="layui-breadcrumb" lay-separator="/">
          <a href="/admin">首页</a>
          <a><cite>文章列表</cite></a>
        </span>
    </blockquote>

    <div class="layui-tab layui-tab-card">
        <form id="articleForm" method="post">
            <input type="hidden" name="currentUrl" id="currentUrl" value="">
            <table id="articleTab" lay-filter="articleTab"></table>


        </form>
        <%--<%@ include file="../Public/paging.jsp" %>--%>
    </div>

</rapid:override>
<rapid:override name="footer-script">
    <script type="text/html" id="toolbar">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-sm" lay-event="add">添加</button>
            <button class="layui-btn layui-btn-sm layui-btn-danger" lay-event="delete">删除</button>
            <%--<button class="layui-btn layui-btn-sm" lay-event="update">编辑</button>--%>
        </div>
    </script>
    <script>
        layui.use(['laydate'], function () {
            var form = layui.form, layer = layui.layer;

            var table = layui.table;
            table.init('articleTab',{
                elem: '#articleTab',
                // height: 312,
                url: 'article/api/queryList' , //数据接口
                page: true ,//开启分页
                toolbar:'#toolbar',
                cols: [[ //表头
                    {type:'checkbox'}
                    ,{field: 'articleId', title: 'ID', width:80, sort: true}
                    ,{field: 'articleTitle', title: '标题', width:150}
                    ,{field: 'tagList', title: '所属分类', width:380, templet:function(record){
                            let dat = record.categoryList;
                            let list =new Array();
                            dat.forEach(function (value) {
                                list.push(value.categoryName);
                            })
                            return list.join(",");
                        }}
                    ,{field: 'tagList', title: '标签', width:250 , templet:function (record) {
                            let dat = record.tagList;
                            let list =new Array();
                            dat.forEach(function (value) {
                                list.push(value.tagName);
                            })
                            return list.join(",");
                        }}
                    ,{field: 'articleStatus', title: '状态', width:80}
                    ,{field: 'articleCreateTime', title: '发布时间',templet:function (record) {
                            var date = new Date(record.articleCreateTime);
                            var y = date.getFullYear();
                            var m = "0"+(date.getMonth()+1);
                            var d = "0"+date.getDate();
                            return y+"-"+m.substring(m.length-2,m.length)+"-"+d.substring(d.length-2,d.length);
                        }}
                    ,{field: 'operator', title: '操作', width: 200, templet:function(record){
                        var editBtn = '<a href="/admin/article/edit/'
                            + record.articleId
                            +'" target="_blank" class="layui-btn layui-btn-mini">编辑</a>';
                        var deleteBtn = '<a href="javascript:void(0)" onclick="deleteArticle('
                            + record.articleId
                            +')" class="layui-btn layui-btn-danger layui-btn-mini">删除</a>';
                        var html = editBtn+'\n'+deleteBtn;
                        return html;
                    }}
                ]],
                parseData: function(res){ //res 即为原始返回的数据
                    return {
                        "status": res.status, //解析接口状态
                        "msg": res.message, //解析提示文本
                        "count": res.data.total, //解析数据长度
                        "data": res.data.list //解析数据列表
                    }
                },
                response: {
                    statusName: 'status' //规定数据状态的字段名称，默认：code
                    ,statusCode: '000000' //规定成功的状态码，默认：0
                    ,msgName: 'hint' //规定状态信息的字段名称，默认：msg
                    // ,countName: 'count' //规定数据总数的字段名称，默认：count
                    // ,dataName: 'rows' //规定数据列表的字段名称，默认：data
                }
            });

            table.on('toolbar', function(obj){
                var checkStatus = table.checkStatus(obj.config.id);
                switch(obj.event){
                    case 'add':
                        window.open('article/insert');
                        // layer.msg('添加')
                        break;
                    case 'delete':
                        var checkStatus = table.checkStatus('articleTab')
                        if (checkStatus.data.length < 1){
                            layer.msg("您没有选中任何行",function(){return false;});
                           return false;
                        }
                        break;
                    case 'update':
                        layer.msg('编辑');
                        break;
                };
            });

        });
    </script>
</rapid:override>
<%@ include file="../Public/framework.jsp" %>
