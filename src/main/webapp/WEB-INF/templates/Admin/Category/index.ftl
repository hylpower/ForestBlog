
<@override name="title">
    - 分类列表
</@override>
<@override name="header-style">
    <style>
        /*覆盖 layui*/
        .layui-input-block {
            margin:0px 10px;
        }
        .layui-table {
            margin-top: 0;
        }
        .layui-col-md4 {
            padding:10px;
        }
        .layui-col-md8 {
            padding:10px;
        }
        .layui-btn {
            margin: 2px 0!important;
        }
    </style>
</@override>

<@override name="content">

    <blockquote class="layui-elem-quote">
        <span class="layui-breadcrumb" lay-separator="/">
              <a href="${forestActive}/admin">首页</a>
              <a href="${forestActive}/admin/category">分类列表</a>
              <a><cite>添加分类</cite></a>
        </span>
    </blockquote>
     <div class="layui-tab layui-tab-card">
         <form id="categoryForm" method="post">
             <input type="hidden" name="currentUrl" id="currentUrl" value="">
             <table id="categoryTab" lay-filter="categoryTab"></table>
         </form>
     </div>

    <#--<div class="layui-row">-->
        <#--<div class="layui-col-md4">-->
            <#--<form class="layui-form"  method="post" id="myForm" action="/admin/category/insertSubmit">-->
                <#--<div class="layui-form-item">-->
                    <#--<div class="layui-input-block">-->
                        <#--<strong>添加分类</strong>-->
                    <#--</div>-->
                    <#--<div class="layui-input-block">-->
                        <#--名称 <span style="color: #FF5722; ">*</span>-->
                        <#--<input type="text" name="categoryName" placeholder="请输入分类名称" autocomplete="off" class="layui-input" required>-->
                    <#--</div>-->
                    <#--<br>-->
                    <#--<div class="layui-input-block">-->
                        <#--父节点 <span style="color: #FF5722; ">*</span>-->
                        <#--<select name="categoryPid" class="layui-input" required>-->
                            <#--<option value="0">无</option>-->
                            <#--<#list categoryList as c>-->

                                <#--<#if c.categoryPid==0>-->
                                    <#--<option value="${c.categoryId}">${c.categoryName}</option>-->
                                <#--</#if>-->
                            <#--</#list>-->
                        <#--</select>-->
                    <#--</div>-->
                    <#--<br>-->
                    <#--<div class="layui-input-block">-->
                        <#--分类描述-->
                        <#--<input type="text" name="categoryDescription" placeholder="请输入分类描述" autocomplete="off" class="layui-input" >-->
                    <#--</div>-->
                    <#--<br>-->
                    <#--<div class="layui-input-block">-->
                        <#--图标样式-->
                        <#--<input type="text" name="categoryIcon" placeholder="请输入图标样式,如 fa fa-coffee" autocomplete="off" class="layui-input" >-->
                    <#--</div>-->
                    <#--<br>-->
                    <#--<div class="layui-input-block">-->
                        <#--<button class="layui-btn" lay-filter="formDemo" type="submit">添加</button>-->
                    <#--</div>-->
                <#--</div>-->
            <#--</form>-->
        <#--</div>-->
        <#--<div class="layui-col-md8" >-->
            <#--<table class="layui-table" >-->
                <#--<colgroup>-->
                    <#--<col width="300">-->
                    <#--<col width="100">-->
                    <#--<col width="100">-->
                    <#--<col width="100">-->
                    <#--<col width="50">-->
                    <#--<col width="50">-->
                <#--</colgroup>-->
                <#--<thead>-->
                <#--<tr>-->
                    <#--<th>名称</th>-->
                    <#--<th>文章数</th>-->
                    <#--<th>操作</th>-->
                    <#--<th>ID</th>-->
                    <#--<th>pid</th>-->
                <#--</tr>-->
                <#--</thead>-->
                <#--<tbody>-->
                    <#--<#list categoryList as c>-->
                        <#--<#if c.categoryPid==0>-->
                        <#--<tr>-->
                            <#--<td>-->
                                <#--<a href="/category/${c.categoryId}" target="_blank">${c.categoryName}</a>-->
                            <#--</td>-->
                            <#--<td>-->
                                <#--<a href="/category/${c.categoryId}" target="_blank">${c.articleCount}</a>-->
                            <#--</td>-->
                            <#--<td>-->
                                <#--<a href="/admin/category/edit/${c.categoryId}" class="layui-btn layui-btn-mini">编辑</a>-->

                                <#--<#if c.articleCount==0>-->
                                    <#--<a href="/admin/category/delete/${c.categoryId}" class="layui-btn layui-btn-danger layui-btn-mini" onclick="return confirmDelete()">删除</a>-->
                                <#--</#if>-->

                            <#--</td>-->
                            <#--<td>${c.categoryId}</td>-->
                            <#--<td>${c.categoryPid}</td>-->
                        <#--</tr>-->

                            <#--<#list categoryList as c2>-->
                                <#--<#if c2.categoryPid == c.categoryId>-->
                                <#--<tr>-->
                                    <#--<td>-->
                                        <#--<a href="/category/${c2.categoryId}" target="_blank">——${c2.categoryName}</a>-->
                                    <#--</td>-->
                                    <#--<td>-->
                                        <#--<a href="/category/${c2.categoryId}" target="_blank">${c2.articleCount}</a>-->
                                    <#--</td>-->
                                    <#--<td>-->
                                        <#--<a href="/admin/category/edit/${c2.categoryId}" class="layui-btn layui-btn-mini">编辑</a>-->
                                        <#--<c:if test="${c2.articleCount==0}">-->
                                            <#--<a href="/admin/category/delete/${c2.categoryId}" class="layui-btn layui-btn-danger layui-btn-mini" onclick="return confirmDelete()">删除</a>-->
                                        <#--</c:if>-->
                                    <#--</td>-->
                                    <#--<td class="cate-parent">${c2.categoryId}</td>-->
                                    <#--<td>${c2.categoryPid}</td>-->
                                <#--</tr>-->
                                <#--</#if>-->

                            <#--</#list>-->

                        <#--</#if>-->


                    <#--</#list>-->

                <#--</tbody>-->
            <#--</table>-->
            <#--<blockquote class="layui-elem-quote layui-quote-nm">-->
                <#--温馨提示：-->
                <#--<ul>-->
                    <#--<li>分类最多只有两级，一级分类pid=0，二级分类pid=其父节点id</li>-->
                    <#--<li>如果该分类包含文章，将不可删除</li>-->
                <#--</ul>-->
            <#--</blockquote>-->
        <#--</div>-->
    <#--</div>-->

</@override>
<@override name="footer-script">
<script type="text/html" id="toolbar">
    <div class="layui-btn-container">
        <a class="layui-btn layui-btn-sm" lay-event="add" onclick="addCategory()">添加</a>
        <a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="delete" onclick="deleteCategory()">删除</a>
    <#--<<button class="layui-btn layui-btn-sm" lay-event="update">编辑</button>-->
    </div>
</script>
<script>
layui.config({
    base : '../../plugin/layui_exts/'
}).extend({
    selectM: 'selectM/selectM',
    treetable:'treetable-lay/treetable'
}).use(['form','layer','treetable','table'], function() {
    var form=layui.form, layer = layui.layer,treetable = layui.treetable,table = layui.table;

    treetable.render({
        treeColIndex: 2,          // treetable新增参数
        treeSpid: 0,             // treetable新增参数
        treeIdName: 'categoryId',       // treetable新增参数
        treePidName: 'categoryPid',     // treetable新增参数
        treeDefaultClose: false,   // treetable新增参数
        treeLinkage: true,        // treetable新增参数
        elem: '#categoryTab',
        // height: 312,
        url: '${forestActive}/admin/category/api/queryList' , //数据接口
        page: true ,//开启分页
        toolbar:'#toolbar',
        cols: [[ //表头
            {type:'checkbox'}
            ,{field: 'categoryId', title: 'ID', width:80}
            ,{field: 'categoryName', title: '名称'}
            ,{field: 'articleCount', title: '文章数', width:80}
            ,{field: 'operator', title: '操作', width: 200, templet:function(record){
                    var editBtn = '<a href="${forestActive}/admin/article/edit/'
                            + record.articleId
                            +'" target="_blank" class="layui-btn layui-btn-xs">编辑</a>';
                    var deleteBtn = '<a href="javascript:void(0)" onclick="deleteCategory('
                            + record.articleId
                            +')" class="layui-btn layui-btn-danger layui-btn-xs">删除</a>';
                    var html = editBtn+'\n'+deleteBtn;
                    return html;
                }}
        ]]
    });

    function addCategory(id,pid){
        layer.open({
            type:2,
            title:false,
            area: ['400px','320px'],
            closeBtn:0,
            shadeClose:true,
            success:function(layero,index){
                let body = layer.getChildFrame('body', index);
                // body.find('#username').val('admin');
                if(id != null && pid != null){
                    body.find('#cid').attr('value',id);
                    body.find('#pid').attr('value',pid);
                }

                // let body = layero.find('body');
            },
            end: function(index, layero){

            },
            content:'${forestActive}/admin/category/edit'
        });
    };

    window.addCategory = addCategory;

    function deleteCategory(){
        var checkStatus = table.checkStatus('categoryTab');
        console.log(checkStatus.data[0].categoryId);
        let id = checkStatus.data[0].categoryId;
        $.ajax({
            type:'post',
            data:{'categoryId':id},
            dataType:"json",
            url:'${forestActive}/admin/category/api/deleteCategory',
            success:function (response) {
                if(response.status == "000000"){
                    layer.msg("删除成功");
                    treetable.render({
                        treeColIndex: 2,          // treetable新增参数
                        treeSpid: 0,             // treetable新增参数
                        treeIdName: 'categoryId',       // treetable新增参数
                        treePidName: 'categoryPid',     // treetable新增参数
                        treeDefaultClose: false,   // treetable新增参数
                        treeLinkage: true,        // treetable新增参数
                        elem: '#categoryTab',
                        // height: 312,
                        url: '${forestActive}/admin/category/api/queryList' , //数据接口
                        page: true ,//开启分页
                        toolbar:'#toolbar',
                        cols: [[ //表头
                            {type:'checkbox'}
                            ,{field: 'categoryId', title: 'ID', width:80}
                            ,{field: 'categoryName', title: '名称'}
                            ,{field: 'articleCount', title: '文章数', width:80}
                            ,{field: 'operator', title: '操作', width: 200, templet:function(record){
                                    var editBtn = '<a href="${forestActive}/admin/article/edit/'
                                            + record.articleId
                                            +'" target="_blank" class="layui-btn layui-btn-xs">编辑</a>';
                                    var deleteBtn = '<a href="javascript:void(0)" onclick="deleteCategory('
                                            + record.articleId
                                            +')" class="layui-btn layui-btn-danger layui-btn-xs">删除</a>';
                                    var html = editBtn+'\n'+deleteBtn;
                                    return html;
                                }}
                        ]]
                    });
                }
            }
        })

    };
    window.deleteCategory = deleteCategory;
});
</script>
</@override>

<@extends name="../Public/framework.ftl" />
