<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>文章上传</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <script type="text/javascript" src="${forestActive}/plugin/layui/layui.js"></script>
    <script type="text/javascript" src="${forestActive}/js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="${forestActive}/plugin/layui/css/layui.css" media="all" />
</head>
<style>
    #tagList .layui-form-select{
        width:380px;
    }
    .categoryEdit{
        margin-top:20px;
    }
</style>
<body class="categoryEdit">
    <form id="upload_form" class="layui-form">
        <div class="layui-container">
            <!-- <blockquote class="layui-elem-quote title magt10"><i class="layui-icon">&#xe609;</i>文章</blockquote> -->

            <input id="cid" type="text" name="cid"  class="layui-input layui-hide">
            <input id="pid" type="text" name="pid"  class="layui-input layui-hide">

            <div class="layui-form-item title-item">
                <label class="layui-form-label">分类名称</label>
                <div class="layui-input-block">
                    <input id="categoryName" type="text" name="categoryName" lay-filter="title" placeholder="请输入名称" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-form-item">
                    <label class="layui-form-label">父节点</label>
                    <div class="layui-input-block">
                        <select id="categoryPidSelect" name="categoryPid" class="layui-input" required>
                            <option value="0">无</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-form-item">
                    <label class="layui-form-label">分类描述</label>
                    <div class="layui-input-block">
                        <input id="categoryDescription" type="text" name="categoryDescription" lay-filter="title" placeholder="请输入名称" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block setting-btn">
                    <a class="layui-btn" lay-submit lay-filter="upload">保存</a>
                    <a id="cancel" class="layui-btn layui-btn-primary">取消</a>
                </div>
            </div>
        </div>
    </form>
</body>
<script>
layui.use(['form'],function () {
    var form = layui.form;

    $(document).ready(function () {
        $.ajax({
            type:'get',
            url: "${forestActive}/admin/category/api/queryList",
            success:function(response){
                if(response.status == '0'||response.status== '000000'){
                    let list = response.data;
                    var html='<option value="0">无</option>';
                    // for(let c in list){
                    for(let j = 0,len=list.length; j < len; j++) {
                        let c = list[j];

                        if (c.categoryPid==0){
                            html += '\n<option value="'+c.categoryId+'">'+c.categoryName+'</option>';
                        }
                    }
                    $('#categoryPidSelect').html(html);
                    form.render('select');
                }
            }
        });
    })

    form.on('submit',function (data) {

        let up_data =data.field;
        $.ajax({
            type:'post',
            url:'${forestActive}/admin/category/api/saveCategory',
            data:up_data,
            dataType:'json',
            success:function (resp) {
                if(resp.status =='000000'){
                    layer.msg("保存成功");
                    parent.layer.closeAll();
                    parent.location.reload();
                    return false;
                }
            }
        })
    })


});
</script>
</html>