<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <link rel="shortcut icon" href="${forestActive}/img/logo.png">
    <title>
    <#if options?? >${options.optionSiteTitle}</#if>后台
        <@block name="title"></@block>
    </title>
    <link rel="stylesheet" href="${forestActive}/plugin/layui/css/layui.css">
    <link rel="stylesheet" href="${forestActive}/css/back.css">
    <link rel="stylesheet" href="${forestActive}/plugin/font-awesome/css/font-awesome.min.css">
    <@block name="header-style"></@block>
    <@block name="header-script"></@block>
</head>
<body>
<div class="layui-layout layui-layout-admin">
    <#--头部区域（可配合layui已有的水平导航）-->
    <div class="layui-header">
        <div class="layui-logo"><a href="/admin" style="color:#009688;">
        <#if options?? >${options.optionSiteTitle}</#if>后台
        </a>
        </div>
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="/" target="_blank">前台</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">新建</a>
                <dl class="layui-nav-child">
                    <dd><a href="${forestActive}/admin/article/insert">文章</a></dd>
                    <dd><a href="${forestActive}/admin/page/insert">页面</a></dd>
                    <dd><a href="${forestActive}/admin/category/insert">分类</a></dd>
                    <dd><a href="${forestActive}/admin/notice/insert">公告</a></dd>
                    <dd><a href="${forestActive}/admin/link/insert">链接</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="${loginUser.userAvatar}" class="layui-nav-img">
                ${loginUser.userName}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="${forestActive}/admin/user/profile/${loginUser.userId}">基本资料</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a href="${forestActive}/admin/logout">退了</a>
            </li>
        </ul>
    </div>

    <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <#--左侧导航区域-->
            <ul class="layui-nav layui-nav-tree"  lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">文章</a>
                    <dl class="layui-nav-child">
                        <dd><a href="${forestActive}/admin/article">全部文章</a></dd>
                        <dd><a href="${forestActive}/admin/article/insert" target="_blank">写文章</a></dd>
                        <dd><a href="${forestActive}/admin/category">全部分类</a></dd>
                        <dd><a href="${forestActive}/admin/tag">全部标签</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">页面</a>
                    <dl class="layui-nav-child">
                        <dd><a href="${forestActive}/admin/page">全部页面</a></dd>
                        <dd><a href="${forestActive}/admin/page/insert">添加页面</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a class="" href="javascript:;">
                        链接
                    </a>
                    <dl class="layui-nav-child">
                        <dd><a href="${forestActive}/admin/link">全部链接</a></dd>
                        <dd><a href="${forestActive}/admin/link/insert">添加链接</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">公告</a>
                    <dl class="layui-nav-child">
                        <dd><a href="${forestActive}/admin/notice">全部公告</a></dd>
                        <dd><a href="${forestActive}/admin/notice/insert">添加公告</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="${forestActive}/admin/comment">
                        评论
                    </a>
                </li>
                <li class="layui-nav-item">
                    <a class="" href="javascript:;">
                        用户
                    </a>
                    <dl class="layui-nav-child">
                        <dd><a href="${forestActive}/admin/user">全部用户</a></dd>
                        <dd><a href="${forestActive}/admin/user/insert">添加用户</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">设置</a>
                    <dl class="layui-nav-child">
                        <dd><a href="${forestActive}/admin/menu">菜单</a></dd>
                        <dd><a href="${forestActive}/admin/options">主要选项</a></dd>
                    </dl>
                </li>
            </ul>
        </div>
    </div>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div style="padding: 15px;">
            <@block name="content">

            </@block>
        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © <#if options?? >${options.optionSiteTitle}</#if> - 后台
    </div>
</div>

<script src="${forestActive}/js/jquery.min.js"></script>
<script src="${forestActive}/plugin/layui/layui.all.js"></script>
<script src="${forestActive}/js/back.js"></script>
<@block name="footer-script">

</@block>

</body>
</html>