<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>markdown编辑器</title>
    <script src="${forestActive}/js/jquery-3.2.1.min.js"></script>
    <script src="${forestActive}/js/marked.js"></script>
    <script src="${forestActive}/plugin/ace/ace.js"></script>
    <script src="${forestActive}/plugin/layui/layui.js"></script>
    <link href="${forestActive}/css/markdown-edit.css" rel="stylesheet">
    <link href="${forestActive}/css/github-markdown.css" rel="stylesheet">
    <link href="${forestActive}/plugin/layui/css/layui.css" rel="stylesheet">
    <link href="${forestActive}/css/mdeditor.css" rel="stylesheet">
    <script src="${forestActive}/js/mdeditor.js"></script>
</head>

<body class="layui-bg-black" >
    <!-- 工具栏 -->
    <form id="toolbar" class="layui-form">
        <input type="text" id="username" hidden="hidden" >
        <div class="layui-row">
            <div class="layui-col-xs8 layui-col-sm4 layui-col-md2">
                <ul>
                    <li></li>
                    <li><i class="layui-icon layui-icon-fonts-strong" onclick="insertTextAround('**','**')" title="加粗" ></i></li>
                    <li><i class="layui-icon layui-icon-fonts-i" onclick="insertTextAround('*','*')" title="斜体" ></i></li>
                    <li></li>
                    <li><i class="layui-icon layui-icon-link" onclick="insertTextAround('[','](url)')" title="引用"></i></li>
                    <li><i class="layui-icon layui-icon-fonts-code" onclick="insertTextAround('\t','')" title="代码"></i></li>
                    <!-- <li><i class="layui-icon layui-icon-fonts-code" onclick="insertText('```\n','\n```')" title="代码"></i></li> -->
                    <li><i class="mine-icon" title="段落引用" onclick="insertTextAround('> ','')">&#60545</i></li>
                    <li></li>
                    <li><i class="mine-icon" title="有序列" onclick="insertTextAround('1. ','')">&#60546</i></li>
                    <li><i class="mine-icon" title="无序列">&#60544</i></li>
                    <li><i class="layui-icon layui-icon-picture" title="图片" id="picture" ></i></li>
                    <li></li>
                    <li></li>
                    
                </ul>
            </div>

            <div class="layui-hide-xs layui-col-sm5 layui-col-md9">
                <#if article??>
                    <input id="title" type="text" name="title" required lay-verify="required"  class="layui-input" value="${article.articleTitle}">
                    <input id="articleId" type="text" name="articleId" class="layui-input layui-hide" value="${article.articleId}">
                    <input id="markdownContent" type="text" name="markdownContent" class="layui-input layui-hide" value="${article.markdownContent}">
                    <input id="articleUserId" type="text" name="articleUserId" class="layui-input layui-hide" value="${article.articleUserId}">
                <#else>
                    <input id="title" type="text" name="title" required lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
                    <input id="articleId" type="text" name="articleId"  class="layui-input layui-hide">
                    <input id="markdownContent" type="text" name="markdownContent"  class="layui-input layui-hide">
                    <input id="articleUserId" type="text" name="articleUserId"  class="layui-input layui-hide">
                </#if>

                <#if firstCategory??>
                    <input id="first-category" type="text" name="first-category" class="layui-input layui-hide" value="${firstCategory.categoryId}">
                <#else>
                    <input id="first-category" type="text" name="first-category" class="layui-input layui-hide">
                </#if>
                <#if secondCategory??>
                    <input id="second-category" type="text" name="second-category" class="layui-input layui-hide" value="${secondCategory.categoryId}">
                <#else>
                    <input id="second-category" type="text" name="second-category" class="layui-input layui-hide">
                </#if>
                <#if tags ??>
                    <input id="tags" type="text" name="tags" class="layui-input layui-hide" value="${tags}">
                <#else>
                    <input id="tags" type="text" name="tags" class="layui-input layui-hide">
                </#if>
            </div>
            <div class="layui-col-xs4 layui-col-sm2 layui-col-md1">
                <ul>
                    <li><i class="layui-icon layui-icon-set" title="设置" id="setting"></i></li>
                    <li id="upload"><i class="layui-icon layui-icon-upload" title="上传" ></i></li>
                </ul>
            </div>
        </div>
        <!-- <div class="layui-inline">
           
            <button class="layui-btn" lay-submit>上传</button>
        </div> -->
        <!-- <button id="scroll" class="layui-btn" >滑动</button> -->

        <!-- https://fly.layui.com/jie/5122/ -->
    </form>
    <div id="container">
        <div id="editor-column" class="pull-left">
            <div id="panel-editor">
                <!--编辑区-->
                <div class="editor-content" id="mdeditor"></div>
            </div>
        </div>
        <div id="preview-column" class="pull-right">
            <div id="panel-preview" >
                <!--显示区-->
                <div id="preview" class="markdown-body"></div>
            </div>
        </div>
    </div>
    

</body>

</html>