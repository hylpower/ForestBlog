<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>文章上传</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <script type="text/javascript" src="${forestActive}/plugin/layui/layui.js"></script>
    <script type="text/javascript" src="${forestActive}/js/mdupload.js"></script>
    <script type="text/javascript" src="${forestActive}/js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="${forestActive}/plugin/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="${forestActive}/css/mdupload.css" media="all" />
    <link href="${forestActive}/css/mdiframe.css" rel="stylesheet">
</head>
<style>
#tagList .layui-form-select{
    width:380px;
}
</style>
<body class="mdiframe">
    <form id="upload_form" class="layui-form">
        <div class="layui-container">
            <!-- <blockquote class="layui-elem-quote title magt10"><i class="layui-icon">&#xe609;</i>文章</blockquote> -->

            <input id="articleId" type="text" name="articleId"  class="layui-input layui-hide">
            <input id="articleUserId" type="text" name="articleUserId"  class="layui-input layui-hide">
            <#--<input id="markdownContent" type="text" name="markdownContent"  class="layui-input layui-hide">-->
            <#--<input id="htmlContent" type="text" name="htmlContent"  class="layui-input layui-hide">-->

            <div class="layui-form-item title-item">
                <label class="layui-form-label">文章标题</label>
                <div class="layui-input-block">
                    <input id="title" type="text" name="title" lay-filter="title" placeholder="请输入标题" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-form-item">
                    <label class="layui-form-label">内容摘要</label>
                    <div class="layui-input-block">
                        <textarea name="desc" lay-filter="description" placeholder="请输入内容" class="layui-textarea abstract-content"></textarea>
                    </div>
                </div>
            </div>
            <!-- 分类 -->
            <div class="layui-form-item">
               
                <label class="layui-form-label">分类</label>
                <div class="layui-input-block">
                    <div class="layui-inline">
                        <select id="first-category" name="first-category" lay-filter="first-category" lay-verify="required">
                        </select>
                    </div>
                    <div class="layui-inline">
                        <select id="second-category" name="second-category" lay-filter="second-category" lay-verify="required">
                        </select>
                    </div>
                </div>

            </div>
            <!-- 标签 -->
            <div class="layui-form-item">

                <label class="layui-form-label">标签</label>
                <div class="layui-input-block">

                    <div class="layui-input-block" id="tagList" style="margin-left:0px;width:380px">
                    <#--<select id="tagList" name="tag" lay-filter="tag" lay-verify="required">-->
                    <#--</select>-->

                </div>

            </div>
            <!-- <blockquote class="layui-elem-quote title magt10"><i class="layui-icon">&#xe609;</i>发布</blockquote> -->
            <div class="layui-form-item">
                <label class="layui-form-label"><i class="layui-icon">&#xe609;</i> 发　布</label>
                <div class="layui-input-block">
                    <input type="radio" name="release" title="立即发布" value="1" lay-skin="primary" lay-filter="release" checked
                        disabled />
                    <input type="radio" name="release" title="定时发布" value="0" lay-skin="primary" lay-filter="release" disabled />
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label"><i class="seraph icon-look"></i>公开度</label>
                <div class="layui-input-block">
                    <input type="radio" name="openness" lay-filter="openness" value="1" title="开放浏览" checked disabled />
                    <input type="radio" name="openness" lay-filter="openness" value="0"title="私密浏览" disabled />
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">置顶</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="switch" lay-filter="stick" lay-skin="switch" disabled>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block setting-btn">
                    <button class="layui-btn" lay-submit lay-filter="upload">上传</button>
                    <button id="cancel" class="layui-btn layui-btn-primary">取消</button>
                </div>
            </div>
            
        </div>
    </form>

</body>

</html>