<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>编辑器设置</title>
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/marked.js"></script>
    <script src="/js/ace/ace.js"></script>
    <script src="/plugin/layui/layui.js"></script>
    <link href="/css/markdown-edit.css" rel="stylesheet">
    <link href="/css/github-markdown.css" rel="stylesheet">
    <link href="/plugin/layui/css/layui.css" rel="stylesheet">
    <link href="/css/mdsetting.css" rel="stylesheet">
    <link href="/css/mdiframe.css" rel="stylesheet">
</head>

<body class="mdiframe">
    <form class="layui-form" id="setting-form" lay-filter="setting-form">
        <input type="text" id="username" hidden="hidden" >
        <div class="layui-container">
            <!-- 主题 -->
            <div class="layui-form-item">
                <label class="layui-form-label">主题</label>
                <div class="layui-input-block">
                    <select name="city" lay-verify="required" lay-filter="theme">
                        <option value="">初始状态</option>
                        <optgroup label="Bright">
                            <option value="ace/theme/chrome">Chrome</option>
                            <option value="ace/theme/clouds">Clouds</option>
                            <option value="ace/theme/crimson_editor">Crimson Editor</option>
                            <option value="ace/theme/dawn">Dawn</option>
                            <option value="ace/theme/dreamweaver">Dreamweaver</option>
                            <option value="ace/theme/eclipse">Eclipse</option>
                            <option value="ace/theme/github" selected="selected">GitHub</option>
                            <option value="ace/theme/iplastic">IPlastic</option>
                            <option value="ace/theme/solarized_light">Solarized Light</option>
                            <option value="ace/theme/textmate">TextMate</option>
                            <option value="ace/theme/tomorrow">Tomorrow</option>
                            <option value="ace/theme/xcode">XCode</option>
                            <option value="ace/theme/kuroir">Kuroir</option>
                            <option value="ace/theme/katzenmilch">KatzenMilch</option>
                            <option value="ace/theme/sqlserver">SQL Server</option>
                        </optgroup>
                        <optgroup label="Dark">
                            <option value="ace/theme/ambiance">Ambiance</option>
                            <option value="ace/theme/chaos">Chaos</option>
                            <option value="ace/theme/clouds_midnight">Clouds Midnight</option>
                            <option value="ace/theme/cobalt">Cobalt</option>
                            <option value="ace/theme/gruvbox">Gruvbox</option>
                            <option value="ace/theme/idle_fingers">idle Fingers</option>
                            <option value="ace/theme/kr_theme">krTheme</option>
                            <option value="ace/theme/merbivore">Merbivore</option>
                            <option value="ace/theme/merbivore_soft">Merbivore Soft</option>
                            <option value="ace/theme/mono_industrial">Mono Industrial</option>
                            <option value="ace/theme/monokai">Monokai</option>
                            <option value="ace/theme/pastel_on_dark">Pastel on dark</option>
                            <option value="ace/theme/solarized_dark">Solarized Dark</option>
                            <option value="ace/theme/terminal">Terminal</option>
                            <option value="ace/theme/tomorrow_night">Tomorrow Night</option>
                            <option value="ace/theme/tomorrow_night_blue">Tomorrow Night Blue</option>
                            <option value="ace/theme/tomorrow_night_bright">Tomorrow Night Bright</option>
                            <option value="ace/theme/tomorrow_night_eighties">Tomorrow Night 80s</option>
                            <option value="ace/theme/twilight">Twilight</option>
                            <option value="ace/theme/vibrant_ink">Vibrant Ink</option>
                        </optgroup>
                    </select>
                </div>
            </div>
            <!-- 字体大小 -->
            <div class="layui-form-item">
                <label class="layui-form-label">字体大小</label>
                <div class="layui-input-block">
                    <select name="fontsize" id="fontsize" lay-filter="font_size">
                        <option value="10px">10px</option>
                        <option value="11px">11px</option>
                        <option value="12px">12px</option>
                        <option value="13px">13px</option>
                        <option value="14px" selected="selected">14px</option>
                        <option value="16px">16px</option>
                        <option value="18px">18px</option>
                        <option value="20px">20px</option>
                        <option value="24px">24px</option>
                    </select>
                </div>
            </div>
            <!-- 代码折行 -->
            <div class="layui-form-item">
                <label class="layui-form-label" >代码折行</label>
                <div class="layui-input-block" >
                    <select name="fold_style" id="fold_style" lay-filter="fold_style">
                        <option value="manual">manual</option>
                        <option value="markbegin" selected="selected">mark begin</option>
                        <option value="markbeginend">mark begin and end</option>
                    </select>
                </div>
            </div>
            <!-- 自动换行 -->
            <div class="layui-form-item">
                <label class="layui-form-label">自动换行</label>
                <div class="layui-input-block" >
                    <select name="soft_wrap" id="soft_wrap" lay-filter="soft_wrap">
                        <option value="off">Off</option>
                        <option value="40">40 Chars</option>
                        <option value="80">80 Chars</option>
                        <option value="free">Free</option>
                    </select>
                </div>
            </div>

            <div class="layui-row layui-col-space1">
                <div class="layui-col-xs6">
                    <!-- 全选样式 -->
                    <label class="layui-form-label" >全选样式</label>
                    <div class="layui-input-block">
                        <input type="checkbox" name="select_style" lay-filter="select_style" lay-text="line|text" lay-skin="switch" >
                    </div>
                </div>
                <div class="layui-col-xs6">
                    <!-- 光标行高光 -->
                    <label class="layui-form-label">光标行高光</label>
                    <div class="layui-input-block">
                        <input type="checkbox" name="highlight_active" lay-filter="highlight_active" lay-skin="switch" >
                    </div>
                </div>
            </div>
            
            <div class="layui-row layui-col-space1">
                <div class="layui-col-xs6">
                    <!-- 显示行号 -->
                    <label class="layui-form-label" >显示行号</label>
                    <div class="layui-input-block">
                        <input type="checkbox" name="show_gutter" lay-filter="show_gutter" lay-skin="switch" >
                    </div>
                </div>
                <div class="layui-col-xs6">
                    <!-- 打印边距 -->
                    <label class="layui-form-label" >打印边距</label>
                    <div class="layui-input-block">
                        <input type="checkbox" name="show_print_margin" lay-filter="show_print_margin" lay-skin="switch" >
                    </div>
                </div>
            </div>
            <div class="layui-form-item setting-btn">
                <button class="layui-btn" lay-submit lay-filter="submit">提交</button>
                <button id="cancel" class="layui-btn layui-btn-primary" lay-submit lay-filter="cancel">还原</button>
            </div>
        </div>
        
    </form>
</body>
<script src="/js/mdsetting.js"></script>

</html>