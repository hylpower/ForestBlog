<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>文章上传</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <script type="text/javascript" src="/plugin/layui/layui.js"></script>
    <script type="text/javascript" src="/js/mdpicture.js"></script>
    <script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="/plugin/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/css/mdpicture.css" media="all" />
    <link href="/css/mdiframe.css" rel="stylesheet">
</head>

<body class="mdiframe">
    <form id="picture_form" class="layui-form">
        <div class="layui-container">
            <!-- <blockquote class="layui-elem-quote title magt10"><i class="layui-icon">&#xe609;</i>文章</blockquote> -->
            <div class="layui-form-item title-item">
                <label class="layui-form-label">请输入图片地址</label>
            </div>
            <div class="layui-form-item title-item">
                    <label class="layui-form-label"><i class="layui-icon layui-icon-picture pic-label"></i></label>
                <div class="layui-input-block" >
                    <input type="text" name="picUrl" lay-filter="title" placeholder="图片url" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item layui-row">
                <div class="layui-col-xs8">
                    <button type="button" class="layui-btn layui-btn-danger" id="uploadBtn"><i class="layui-icon"></i>上传本地图片</button>
                </div>
                <div class="layui-col-xs4">
                    <button class="layui-btn" lay-submit lay-filter="upload">上传</button>
                    <button id="cancel" class="layui-btn layui-btn-primary">取消</button>
                </div>
            </div>
           
          
            
        </div>
    </form>

</body>

</html>