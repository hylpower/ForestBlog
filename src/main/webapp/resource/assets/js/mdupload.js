layui.config({
    base : '../../plugin/layui_exts/'
}).extend({
    selectM: 'selectM/selectM',
}).use(['form','layer','selectM'],function(){
    var form = layui.form;
    var acen_edit = parent.acen_edit;
    var selectM = layui.selectM;

    $(document).ready(function(){
        initForm();
    });

    form.on('select(first-category)',function(data){
        $.ajax({
            type:"get",
            url:"api/getCategory",
            dataType:"json",
            success:function(response){
                var secondSelectHtml = '<option value="">二级分类</option>';
                if(response.status == "000000"){
                    var dataList = response.data;
                    dataList.forEach(function(val,index){
                        if (val.categoryPid == data.value){
                            secondSelectHtml += '\n<option value="'+val.categoryId +'">'+val.categoryName+'</option>'
                        }
                    });
                    $('#second-category').html(secondSelectHtml);
                    form.render('select');
                }
            }
        })
    });

    form.on('submit(upload)',function(data){
        var mdContent = acen_edit.getValue();
        // $(data.field);
        // console.log("ok");

        up_data={
            'mdContent':mdContent,
            'htmlContent':parent.$("#preview").html(),
            'field':data.field
        };
        let urlBase = '/admin/article/api/';
        let method;
        //编辑文章提交
        if( data.field.articleId !=null && data.field.articleId != ''){
            method = 'editSubmit';
        }else{
            method = 'addSubmit';
        }

        $.ajax({
            url: urlBase+method,
            type:'POST',
            contentType:"application/json; charset=utf-8",
            data:JSON.stringify(up_data),
            dataType:'json',
            success:function(result){
                if (result.status=='000000'){
                    layer.alert('上传成功',{
                        closeBtn: 0,
                        btn: '确认',
                        yes:function(){
                            closeParentLayer();
                        }
                    });
                }
            },

        });
        return false;
    });
    
    function initForm(){
        let title=$("#title",parent.document).val();
        $("#title").val(title);
        let tags = $('#tags',parent.document).val();
        let tagLis = tags.split(",");
        tagLis.forEach(function (value,index,array) {
            tagLis[index] = parseInt(value);
        })
        //若为编辑文章,则需要去获取原始记录
        let articleId = $('#articleId',parent.document).val();
        if(articleId != null &&articleId != ''){
            $('#articleId').val(articleId);
            $('#articleUserId').val($('#articleUserId',parent.document).val());
            // $('#markdownContent').val($('#markdownContent',parent.document).val());
        }

        //若为新建文章 则需要创建 以新建文章为例
        $.ajax({
            type:"get",
            url:"api/getCategory",
            dataType:"json",
            success:function(response){
                let firstSelectHtml = '<option value="">一级分类</option>';
                let oriFirstSelectedVal = $('#first-category',parent.document).val();
                let oriSecondSelectedVal = $('#second-category',parent.document).val();

                if(response.status == "000000"){
                    let categoryList = response.data;
                    categoryList.forEach(function(val,index){
                        if(val.categoryPid == 0){
                            if(val.categoryId == oriFirstSelectedVal){
                                firstSelectHtml += '\n<option value="'+val.categoryId +'" selected>'+val.categoryName+'</option>';
                                initSecondSelection(categoryList,val.categoryId,oriSecondSelectedVal);
                            }else{
                                firstSelectHtml += '\n<option value="'+val.categoryId +'">'+val.categoryName+'</option>';
                            }
                        }
                    });
                    $('#first-category').html(firstSelectHtml);
                    form.render('select');
                }
            }
        })

        initTagSelect(tagLis);
    }

    /**
     * 若为编辑文章 则需要去获取二级分类原始记录
     * @param dataList 目录数组
     * @param parentId 一级分类Id
     * @param target 目标Id
     */
    function initSecondSelection(dataList,parentId,target){
        let secondSelectHtml = '<option value="">二级分类</option>';
        dataList.forEach(function(val,index){
             if(val.categoryPid == parentId.toString()){
                 if(val.categoryId == target.toString()){
                    secondSelectHtml += '\n<option value="'+val.categoryId +'" selected>'+val.categoryName+'</option>';
                 }else{
                    secondSelectHtml += '\n<option value="'+val.categoryId +'">'+val.categoryName+'</option>';
                 }
             }
        });
        $('#second-category').html(secondSelectHtml);
        form.render('select');
    }

    function initTagSelect(tagLis){
        $.ajax({
            type:"get",
            url:"api/getTagList",
            dataType:"json",
            success:function(response){
                if(response.status == "000000"){
                    var tagList = response.data;
                    var tagListdat = new Array();
                    tagList.forEach(function(value , index , array){
                        var item = {
                            id:value.tagId,
                            name:value.tagName
                        };
                        tagListdat.push(item);
                    });

                    //多选标签-所有配置
                    var tagList = selectM({
                        elem: '#tagList'//元素容器【必填】
                        ,data: tagListdat//候选数据【必填】
                        ,selected: tagLis==null?false:tagLis //默认值
                        ,max : 3 //最多选中个数，默认5
                        ,name: 'tags' //input的name 不设置与选择器相同(去#.)
                        ,delimiter: ',' //值的分隔符
                        ,field: {idName:'id',titleName:'name'} //候选项数据的键名
                        ,showSelectedTitle:false
                    });
                }
            }
        })

    }

    $('button#cancel').on('click',function(){
        closeParentLayer();
    });

    function closeParentLayer(){
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    }
});