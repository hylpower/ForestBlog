layui.use(['form','layer'], function () {
    var form = layui.form;
    var $ = jQuery = layui.$;
    acen_edit = parent.acen_edit;

    oriEditorSettingData = {};
    oriFormSettingData = {};
    $(document).ready(function(){
        oriEditorSettingData = getEditorSettingData();
        oriFormSettingData = parseToFormData(oriEditorSettingData);
        initFormSetting(oriFormSettingData);
    }); 

    function getEditorSettingData(){
        let data = {
            "city": acen_edit.getTheme(),
            "fontsize": acen_edit.getFontSize(),
            "fold_style": acen_edit.getSession().$foldStyle,
            "soft_wrap": acen_edit.getOption("wrap"),
            "select_style": acen_edit.getOption('selectionStyle'),
            "highlight_active": acen_edit.getHighlightActiveLine(),
            "show_gutter": acen_edit.renderer.$showGutter,
            "show_print_margin": acen_edit.getShowPrintMargin()
        }
        return data;
    }

    function parseToFormData(editorSettingData){
        let formData = {}
        for (var key in editorSettingData){
            formData[key] = editorSettingData[key];
        }
        formData['select_style'] = formData['select_style'] == 'line'?true:false;
        return formData;
    }

    form.on('submit(cancel)',function(data){
        initFormSetting(oriFormSettingData);
        parent.initEditor(oriEditorSettingData);
        return false;
    });

    /**
     * 根据编辑器设定进行表单初始化设定 
     */
    function initFormSetting(formData){

        //表单初始化得放在程序最后执行
        form.val('setting-form',formData);
    } 

    form.on('select(theme)', function (data) {
        acen_edit.setTheme(data.value);
    });

    form.on('select(font_size)', function (data) {
        acen_edit.setFontSize(data.value);
    });

    form.on('select(fold_style)', function (data) {
        acen_edit.getSession().setFoldStyle(data.value);
        
    });
    
    form.on('select(soft_wrap)', function (data) {
        acen_edit.setOption("wrap",data.value);
        
    });
    
    form.on('switch(select_style)', function (data) {
        acen_edit.setOption('selectionStyle',data.elem.checked?"line":"text");     
    });

    form.on('switch(highlight_active)', function (data) {
        acen_edit.setHighlightActiveLine(data.elem.checked);     
    });

    form.on('switch(show_gutter)', function (data) {
        acen_edit.renderer.setShowGutter(data.elem.checked);     
    });
    
    form.on('switch(show_print_margin)', function (data) {
        acen_edit.setShowPrintMargin(data.elem.checked);     
    });

    form.on('submit(submit)',function(data){
        layer.msg("提交");
        return false;
    });
    
});