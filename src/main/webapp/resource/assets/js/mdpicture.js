layui.use(['upload','form'],function(){
    var upload = layui.upload;
    var form = layui.form;
    var acen_edit = parent.acen_edit;
    //执行实例
    var uploadInst = upload.render({
        elem: '#uploadBtn' //绑定元素
        ,url: '../../uploadPicture' //上传接口
        // ,accept: 'file'
        ,size:1024
        ,done: function(res, index, upload){
        //上传完毕回调
            if(res.status == '000000'){
                let data = res.data;
                let src = data.src;
                insertPicture(src);

                var index = parent.layer.getFrameIndex(window.name);
                parent.layer.close(index);
                return false;
            }
        }
        ,error: function(){
        //请求异常回调
        }
    });

    form.on('submit(upload)',function(data){
        let field = data.field;
        let picUrl = field['picUrl'];

        insertPicture(picUrl);
        var index = parent.layer.getFrameIndex(window.name); 
        parent.layer.close(index);
        return false;
    });

    function insertPicture(url,desc){
        let picUrl = url;
        if (desc == null){
            desc = "图片描述";
        }
        let ctxt = '!['+desc+']('+picUrl+')';
        parent.insertTextWithContext(ctxt);
    }

    $('#cancel').on('click',function(){
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
        return false;
    });
});