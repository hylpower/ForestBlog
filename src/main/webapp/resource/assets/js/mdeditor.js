layui.config({
    base : '../../plugin/layui_exts/'
}).extend({
    selectM: 'selectM/selectM',
}).use(['form','layer'], function() {
    var form = layui.form;
    // var $ = jQuery = layui.$;
    acen_edit = ace.edit('mdeditor');

    $(document).ready(function() {
        initPage();
        initUserSettingData("default");

        $("div.ace_scrollbar").on('scroll', function(){
            $preview = $("div#preview-column");
            var percentage = this.scrollTop / (this.scrollHeight - this.offsetHeight);
            var height = percentage * ($preview.get(0).scrollHeight - $preview.get(0).offsetHeight);
            $preview.scrollTop(height);
           
        });

    });

    $('#picture').on('click',function(){
        layer.open({
            type:2,
            title:false,
            area:['420px','130px'],
            closeBtn:0,
            shadeClose:true,
            success:function(layero,index){
                let body = layer.getChildFrame('body', index);

                body.find('#username').attr('value','admin');
            },
            content:"./mdpicture"
        });
    });

    $('#setting').on('click',function(){
        layer.open({
            type:2,
            title:false,
            area: ['300px','320px'],
            closeBtn:0,
            shadeClose:true,
            success:function(layero,index){
                let body = layer.getChildFrame('body', index);
                // body.find('#username').val('admin');
                body.find('#username').attr('value','admin');
                // let body = layero.find('body');
            },
            end: function(index, layero){
                layer.msg('保存用户设置');
                let userSettingData = getCurrentEditorSettingData();
                //调用接口保存用户设置
            },
            content:'./mdsetting'
        });
        return false;
    });

    $('#upload').on('click',function(){
        layer.open({
            type:2,
            title:false,
            area:['530px', '500px'],
            cancel: function(index, layero){ 
                layer.confirm('确定要取消上传么', {
                    btn: ['确定','取消'] //按钮
                },function(indexc,layero){
                    layer.close(index);
                    layer.close(indexc);
                },function(){
                    return false;
                });
                return false;
            },
            content:'/admin/article/mdupload'
        });
        return false;
    });

    $("#mdeditor").keyup(function() {
        $("#preview").html(marked(acen_edit.getValue()));
    });

    /**
     * 获取当前编辑器设置参数
     */
    function getCurrentEditorSettingData(){
        let settingData = {
            "city": acen_edit.getTheme(),
            "fontsize": acen_edit.getFontSize(),
            "fold_style": acen_edit.getSession().$foldStyle,
            "soft_wrap": acen_edit.getOption("wrap"),
            "select_style": acen_edit.getOption('selectionStyle'),
            "highlight_active": acen_edit.getHighlightActiveLine(),
            "show_gutter": acen_edit.renderer.$showGutter,
            "show_print_margin": acen_edit.getShowPrintMargin()
        }
        return settingData;
    }

    function initPage(){
        if($('#markdownContent').val() != null){
            acen_edit.setValue($('#markdownContent').val());
            $("#preview").html(marked(acen_edit.getValue()));
        }
    }

    /**
     * 初始化编译器设置
     */
    function initUserSettingData(pid){
        dat = pid;
        $.ajax({
            type: "get",
            url: "json/userSetting.json",
            data: {'username':dat},
            dataType: "json",
            success: function (response) {
                let resp = response[dat];
                initEditor(resp);
            }
        });
        
    }


    window.initEditor = function(data){
        acen_edit.setTheme(data["city"]);
        acen_edit.setFontSize(data["fontsize"]);
        acen_edit.getSession().setFoldStyle(data["fold_style"]);
        acen_edit.setOption("wrap",data["soft_wrap"]);
        acen_edit.setOption('selectionStyle',data["select_style"]?"line":"text");
        acen_edit.setHighlightActiveLine(data["highlight_active"]);
        acen_edit.renderer.setShowGutter(data["show_gutter"]);
        acen_edit.setShowPrintMargin(data["show_print_margin"]);
    }

    window.insertTextAround =function(pre,suf) {
        var txt = window.getSelection()?window.getSelection().toString():document.selection.createRange().text;
        let ctxt = pre+txt+suf;
        insertTextWithContext(ctxt);
    
        // // $('.ace_content')
        // // var currentCursor = window.getSelection().setSelectionRange(0, suf.length);
    };

    window.insertTextWithContext =function(ctxt) {
        acen_edit.insert(ctxt); //光标位置插入
        // var currentCursor = window.getSelection().setSelectionRange(0, suf.length);
        $("#preview").html(marked(acen_edit.getValue()));
    };

});
