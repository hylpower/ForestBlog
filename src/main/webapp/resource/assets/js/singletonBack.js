layui.config({
    base : '../../plugin/layui_exts/'
}).extend({
    selectM: 'selectM/selectM',
    echarts: 'echarts/echarts',
}).use(['form','layer','echarts'], function() {
    var $ = layui.$;
    //记得这是dom对象不是JQ对象，需要转换
    echarts = layui.echarts.init($('#main')[0]);

    //获取地图数据
    $.get("data/pieData.json",
        function (option) {
            //初始化地图
            echarts.setOption(option);
        }
    )

    var option = { //参数
        title: {
            text: 'ECharts 入门示例'
        },
        tooltip: {},
        legend: {
            data:['销量']
        },
        xAxis: {
            data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
        },
        yAxis: {},
        series: [{
            name: '销量',
            type: 'bar',//图类型
            data: [5, 20, 36, 10, 10, 20]
        }]
    };
    myChart.setOption(option);
})