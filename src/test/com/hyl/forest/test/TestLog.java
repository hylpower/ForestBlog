package com.hyl.forest.test;

import com.hyl.forest.test.bean.Person;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Slf4j
public class TestLog {
    @Test
    public void testSlf4j() {
        Logger logger = LoggerFactory.getLogger(Object.class);
       while(true){
           logger.trace("=====trace=====");
           logger.debug("=====debug=====");
           logger.info("=====info=====");
           logger.warn("=====warn=====");
           logger.error("=====error=====");
           try {
               Thread.sleep(2*1000);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
       }
    }

    @Test
    public void applicationContext(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("file:D:\\test_workspace\\ForestBlog\\src\\test\\com\\hyl\\forest\\test\\spring-text.xml");
        Person person = (Person)applicationContext.getBean("person");
        person.setAge(11);
        person = (Person)applicationContext.getBean("person");
        log.info("中文测试[{}]",person.getAge());
        //        Assert.assertTrue(person instanceof Person);
    }
}
